## qssi-user 12
12 SKQ1.210908.001 V13.0.3.0.SKFEUXM release-keys
- Manufacturer: xiaomi
- Platform: sm6150
- Codename: sweet
- Brand: Redmi
- Flavor: qssi-user
- Release Version: 12
12
- Id: SKQ1.210908.001
- Incremental: V13.0.3.0.SKFEUXM
- Tags: release-keys
- CPU Abilist: arm64-v8a,armeabi-v7a,armeabi
- A/B Device: false
- Locale: en-GB
- Screen Density: 440
- Fingerprint: Redmi/sweet_eea/sweet:12/RKQ1.210614.002/V13.0.3.0.SKFEUXM:user/release-keys
- OTA version: 
- Branch: qssi-user-12
12-SKQ1.210908.001-V13.0.3.0.SKFEUXM-release-keys
- Repo: redmi/sweet
